# Brazilian Portuguese translation for Debian website organization.pot
# Copyright (C) 2003-2017 Software in the Public Interest, Inc. 
#
# Michelle Ribeiro <michelle@cipsga.org.br>, 2003
# Gustavo R. Montesino <grmontesino@ig.com.br>, 2004
# Felipe Augusto van de Wiel (faw) <faw@debian.org>, 2006-2009
# Marcelo Gomes de Santana <marcelo@msantana.eng.br>, 2011-2017.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian Webwml\n"
"PO-Revision-Date: 2017-02-28 21:53-0300\n"
"Last-Translator: Marcelo Gomes de Santana <marcelo@msantana.eng.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "mensagem de delegação"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "mensagem de nomeação"

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegado"

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegada"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "atual"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "membro"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "gerente"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr "Gerente de Lançamento da Estável"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "mago"

#: ../../english/intro/organization.data:39
msgid "chairman"
msgstr "presidente"

#: ../../english/intro/organization.data:42
msgid "assistant"
msgstr "assistente"

#: ../../english/intro/organization.data:44
msgid "secretary"
msgstr "secretário"

#: ../../english/intro/organization.data:53
#: ../../english/intro/organization.data:63
msgid "Officers"
msgstr "Oficiais"

#: ../../english/intro/organization.data:54
#: ../../english/intro/organization.data:80
msgid "Distribution"
msgstr "Distribuição"

#: ../../english/intro/organization.data:55
#: ../../english/intro/organization.data:224
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:56
#: ../../english/intro/organization.data:227
msgid "Publicity team"
msgstr "Equipe de publicidade"

#: ../../english/intro/organization.data:57
#: ../../english/intro/organization.data:295
msgid "Support and Infrastructure"
msgstr "Suporte e Infra-estrutura"

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:59
msgid "Debian Pure Blends"
msgstr "Debian Pure Blends"

#: ../../english/intro/organization.data:66
msgid "Leader"
msgstr "Líder"

#: ../../english/intro/organization.data:68
msgid "Technical Committee"
msgstr "Comitê Técnico"

#: ../../english/intro/organization.data:75
msgid "Secretary"
msgstr "Secretário"

#: ../../english/intro/organization.data:83
msgid "Development Projects"
msgstr "Projetos de Desenvolvimento"

#: ../../english/intro/organization.data:84
msgid "FTP Archives"
msgstr "Repositórios FTP"

#: ../../english/intro/organization.data:86
msgid "FTP Masters"
msgstr "FTP Masters"

#: ../../english/intro/organization.data:90
msgid "FTP Assistants"
msgstr "Assistentes FTP"

#: ../../english/intro/organization.data:99
msgid "FTP Wizards"
msgstr "Assistentes FTP"

#: ../../english/intro/organization.data:103
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:105
msgid "Backports Team"
msgstr "Equipe de Backports"

#: ../../english/intro/organization.data:109
msgid "Individual Packages"
msgstr "Pacotes Individuais"

#: ../../english/intro/organization.data:110
msgid "Release Management"
msgstr "Gerência de Lançamento"

#: ../../english/intro/organization.data:112
msgid "Release Team"
msgstr "Equipe de Lançamento"

#: ../../english/intro/organization.data:125
msgid "Quality Assurance"
msgstr "Controle de Qualidade"

#: ../../english/intro/organization.data:126
msgid "Installation System Team"
msgstr "Equipe do Sistema de Instalação"

#: ../../english/intro/organization.data:127
msgid "Release Notes"
msgstr "Notas de Lançamento"

#: ../../english/intro/organization.data:129
msgid "CD Images"
msgstr "Imagens de CDs"

#: ../../english/intro/organization.data:131
msgid "Production"
msgstr "Produção"

#: ../../english/intro/organization.data:139
msgid "Testing"
msgstr "Testing"

#: ../../english/intro/organization.data:141
msgid "Autobuilding infrastructure"
msgstr "Infra-estrutura de auto-construção"

#: ../../english/intro/organization.data:143
msgid "Wanna-build team"
msgstr "Equipe wanna-build"

#: ../../english/intro/organization.data:151
msgid "Buildd administration"
msgstr "Administração de Buildd"

#: ../../english/intro/organization.data:170
msgid "Documentation"
msgstr "Documentação"

#: ../../english/intro/organization.data:175
msgid "Work-Needing and Prospective Packages list"
msgstr "Lista de Pacotes Prospectivos e que necessitam de trabalho"

#: ../../english/intro/organization.data:178
msgid "Live System Team"
msgstr "Equipe do Sistema \"Live\""

#: ../../english/intro/organization.data:179
msgid "Ports"
msgstr "Portes"

#: ../../english/intro/organization.data:214
msgid "Special Configurations"
msgstr "Configurações Especiais"

#: ../../english/intro/organization.data:217
msgid "Laptops"
msgstr "Laptops"

#: ../../english/intro/organization.data:218
msgid "Firewalls"
msgstr "Firewalls"

#: ../../english/intro/organization.data:219
msgid "Embedded systems"
msgstr "Sistemas embarcados/embutidos"

#: ../../english/intro/organization.data:232
msgid "Press Contact"
msgstr "Contato de Imprensa"

#: ../../english/intro/organization.data:234
msgid "Web Pages"
msgstr "Páginas Web"

#: ../../english/intro/organization.data:244
msgid "Planet Debian"
msgstr "Planeta Debian"

#: ../../english/intro/organization.data:249
msgid "Outreach"
msgstr "Programa de extensão"

#: ../../english/intro/organization.data:253
msgid "Debian Women Project"
msgstr "Projeto Debian Women"

#: ../../english/intro/organization.data:261
msgid "Anti-harassment"
msgstr "Antiassédio"

#: ../../english/intro/organization.data:267
msgid "Events"
msgstr "Eventos"

#: ../../english/intro/organization.data:273
msgid "DebConf Committee"
msgstr "Comitê da DebConf"

#: ../../english/intro/organization.data:280
msgid "Partner Program"
msgstr "Programa de Parcerias"

#: ../../english/intro/organization.data:285
msgid "Hardware Donations Coordination"
msgstr "Coordenação de Doações de Hardware"

#: ../../english/intro/organization.data:298
msgid "User support"
msgstr "Suporte ao usuário"

#: ../../english/intro/organization.data:365
msgid "Bug Tracking System"
msgstr "Sistema de Acompanhamento de Bugs"

#: ../../english/intro/organization.data:370
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr ""
"Administração das Listas de Discussão e dos Arquivos das Listas de Discussão"

#: ../../english/intro/organization.data:378
msgid "New Members Front Desk"
msgstr "Recepção de Novos Mantenedores"

#: ../../english/intro/organization.data:384
msgid "Debian Account Managers"
msgstr "Gerentes de Contas Debian"

#: ../../english/intro/organization.data:389
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Para enviar uma mensagem privada para todos os DAMs, use a chave GPG "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:390
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Mantenedores do Chaveiro (Keyring) (PGP e GPG)"

#: ../../english/intro/organization.data:393
msgid "Security Team"
msgstr "Equipe de Segurança"

#: ../../english/intro/organization.data:406
msgid "Consultants Page"
msgstr "Página de Consultores"

#: ../../english/intro/organization.data:411
msgid "CD Vendors Page"
msgstr "Página de Vendedores de CD"

#: ../../english/intro/organization.data:414
msgid "Policy"
msgstr "Política (Policy)"

#: ../../english/intro/organization.data:419
msgid "System Administration"
msgstr "Administração do Sistema"

#: ../../english/intro/organization.data:420
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Este é o endereço que se deve usar quando você encontrar problemas em uma "
"das máquinas do Debian, incluindo problemas de senha ou em caso de você "
"precisar que um determinado pacote seja instalado numa das máquinas do "
"Projeto."

#: ../../english/intro/organization.data:430
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Se você tiver problemas com o hardware de máquinas do Debian, veja a página "
"de <a href=\"https://db.debian.org/machines.cgi\">Máquinas do Debian</a>, "
"ela contém informações sobre o administrador de cada máquina."

#: ../../english/intro/organization.data:431
msgid "LDAP Developer Directory Administrator"
msgstr "Administrador do Diretório LDAP de Desenvolvedores"

#: ../../english/intro/organization.data:432
msgid "Mirrors"
msgstr "Espelhos"

#: ../../english/intro/organization.data:437
msgid "DNS Maintainer"
msgstr "Mantenedor do DNS"

#: ../../english/intro/organization.data:438
msgid "Package Tracking System"
msgstr "Sistema de Acompanhamento de Pacotes"

#: ../../english/intro/organization.data:440
msgid "Auditor"
msgstr "Auditor"

#: ../../english/intro/organization.data:446
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Pedidos de uso da <a name=\"trademark\" href=\"m4_HOME/trademark\">marca</a>"

#: ../../english/intro/organization.data:449
msgid "Alioth administrators"
msgstr "Administração do Alioth"

#: ../../english/intro/organization.data:462
msgid "Debian for children from 1 to 99"
msgstr "Debian para crianças de 1 a 99"

#: ../../english/intro/organization.data:465
msgid "Debian for medical practice and research"
msgstr "Debian para prática e pesquisa médica"

#: ../../english/intro/organization.data:468
msgid "Debian for education"
msgstr "Debian para educação"

#: ../../english/intro/organization.data:473
msgid "Debian in legal offices"
msgstr "Debian em escritórios legais"

#: ../../english/intro/organization.data:477
msgid "Debian for people with disabilities"
msgstr "Debian para pessoas com deficiências"

#: ../../english/intro/organization.data:481
msgid "Debian for science and related research"
msgstr "Debian para ciência e pesquisa relacionada"

#~ msgid "Publicity"
#~ msgstr "Publicidade"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr ""
#~ "Mantenedores do Chaveiro (Keyring) de Mantenedores Debian (Debian "
#~ "Maintainer)"

#~ msgid "current Debian Project Leader"
#~ msgstr "atual Líder do Projeto Debian"

#~ msgid "Testing Security Team"
#~ msgstr "Equipe de Segurança da Testing"

#~ msgid "Security Audit Project"
#~ msgstr "Projeto de Auditoria de Segurança"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Distribuição Personalizada Debian"

#~ msgid "Release Team for ``stable''"
#~ msgstr "Equipe de Lançamento para ``estável'' (stable)"

#~ msgid "Handhelds"
#~ msgstr "Handhelds"

#~ msgid "Marketing Team"
#~ msgstr "Equipe de Marketing"

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Os administradores responsáveis pelas buildds para uma arquitetura em "
#~ "particular podem ser contatados em <genericemail arch@buildd.debian.org>, "
#~ "por exemplo, <genericemail i386@buildd.debian.org>."

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Nomes dos administradores das buildds também podem ser encontrados em <a "
#~ "href=\"http://www.buildd.net\">http://www.buildd.net</a>. Escolha uma "
#~ "arquitetura e a distribuição para ver as buildds disponíveis e seus "
#~ "administradores."

#~ msgid "Key Signing Coordination"
#~ msgstr "Coordenação de Assinatura de Chaves"

#~ msgid "Accountant"
#~ msgstr "Contador"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "O Sistema Operacional Universal como seu Desktop"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian para organizações sem fins lucrativos"

#~ msgid "Mailing List Archives"
#~ msgstr "Arquivos das Listas de Discussão"

#~ msgid "Installation System for ``stable''"
#~ msgstr "Sistema de Instalação para ``estável''"

#~ msgid "Debian GNU/Linux for Enterprise Computing"
#~ msgstr "Debian GNU/Linux para Computação Empresarial"

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Este ainda não é um projeto interno oficial do Debian mas anunciou a "
#~ "intenção de ser integrado."

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "Distribuição Debian Multimídia"

#~ msgid "Release Wizard"
#~ msgstr "Mago de Lançamento"

#~ msgid "Release Assistants for ``stable''"
#~ msgstr "Assistentes de Lançamento para ``estável'' (stable)"

#~ msgid "Release Assistants"
#~ msgstr "Assistentes de Lançamento"

#~ msgid "APT Team"
#~ msgstr "Equipe do APT"

#~ msgid "Vendors"
#~ msgstr "Distribuidores"

#~ msgid "Volatile Team"
#~ msgstr "Equipe da Volatile"

#~ msgid "Alpha (Not active: was not released with squeeze)"
#~ msgstr "Alpha (Não ativo: não foi lançado com o squeeze)"

#~ msgid "DebConf chairs"
#~ msgstr "Presidentes da DebConf"

#~ msgid "Bits from Debian"
#~ msgstr "Breves do Debian"
