#use wml::debian::blends::gis
#use wml::debian::translation-check translation="7b4931ea1aec4a215fb6f5cb93bf4da72cbb97e2" maintainer="Ana Custura"
{#alternate_navbar#:
   <div id="second-nav">
      <p><a href="$(HOME)/blends/gis/">Debian&nbsp;GIS&nbsp;Blend</a></p>
    <ul>
      <li><a href="$(HOME)/blends/gis/about">Despre Debian GIS</a></li>
      <li><a href="$(HOME)/blends/gis/contact">Contact</a></li>
      <li><a href="$(HOME)/blends/gis/get/">Obține&nbsp;Blend-ul</a>
         <ul>
         <li><a href="$(HOME)/blends/gis/get/live">Descarcă&nbsp;Imagini&nbsp;live</a></li>
         <li><a href="$(HOME)/blends/gis/get/metapackages">Pachete&nbsp;meta</a></li>
	 </ul>
      </li>
      <li><a href="$(HOME)/blends/gis/support">Suport</a></li>
      <li><a href="$(HOME)/blends/gis/deriv">Derivative</a></li>
      <li><a href="https://wiki.debian.org/DebianGIS#Development">Pentru programatori</a>
        <ul>
        <li><a href="<gis-policy-html/>">Regulamentul echipei GIS</a></li>
        </ul>
      </li>
      <li><a href="https://twitter.com/DebianGIS"><img src="$(HOME)/blends/gis/Pics/twitter.gif" alt="Twitter" width="80" height="15"></a></li>
    </ul>
   </div>
:#alternate_navbar#}
