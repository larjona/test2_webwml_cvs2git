# SOME DESCRIPTIVE TITLE.
# Copyright ©
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-08-28 22:46+0700\n"
"Last-Translator: Izharul Haq <atoz.chevara@yahoo.com>\n"
"Language-Team: L10N Debian Indonesian <debian-l10n-indonesian@lists.debian."
"org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Statistik penerjemahan web site Debian"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Ada %d halaman-halaman untuk diterjemahkan."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Ada %d bytes untuk diterjemahkan."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Ada %d strings untuk diterjemahkan."

#: ../../stattrans.pl:229 ../../stattrans.pl:233
msgid "This translation is too out of date"
msgstr "Terjemahan ini terlalu ketinggalan zaman"

#: ../../stattrans.pl:231 ../../stattrans.pl:427
msgid "Wrong translation version"
msgstr "Versi terjemahan yang salah"

#: ../../stattrans.pl:235
msgid "The original is newer than this translation"
msgstr "Aslinya lebih baru dari pada terjemahan ini"

#: ../../stattrans.pl:238 ../../stattrans.pl:427
msgid "The original no longer exists"
msgstr "Aslinya tidak ada lagi"

#: ../../stattrans.pl:403
msgid "hit count N/A"
msgstr "jumlah hit N/A"

#: ../../stattrans.pl:403
msgid "hits"
msgstr "hit"

#: ../../stattrans.pl:421 ../../stattrans.pl:422
msgid "Click to fetch diffstat data"
msgstr "Klik untuk mengambil data diffstat"

#: ../../stattrans.pl:436 ../../stattrans.pl:580 ../../stattrans.pl:581
msgid "Unified diff"
msgstr "Unified diff"

#: ../../stattrans.pl:439 ../../stattrans.pl:580 ../../stattrans.pl:581
msgid "Colored diff"
msgstr "Berwarna diff"

#: ../../stattrans.pl:541 ../../stattrans.pl:683
msgid "Created with <transstatslink>"
msgstr ""

#: ../../stattrans.pl:546
msgid "Translation summary for"
msgstr "Ringkasan terjemahan untuk"

#: ../../stattrans.pl:549 ../../stattrans.pl:707 ../../stattrans.pl:753
#: ../../stattrans.pl:796
msgid "Not translated"
msgstr "Tidak diterjemahkan"

#: ../../stattrans.pl:549 ../../stattrans.pl:706 ../../stattrans.pl:752
msgid "Outdated"
msgstr "Telah usang"

#: ../../stattrans.pl:549
msgid "Translated"
msgstr "Telah diterjemahkan"

#: ../../stattrans.pl:549 ../../stattrans.pl:631 ../../stattrans.pl:705
#: ../../stattrans.pl:751 ../../stattrans.pl:794
msgid "Up to date"
msgstr "Mutakhir"

#: ../../stattrans.pl:550 ../../stattrans.pl:551 ../../stattrans.pl:552
#: ../../stattrans.pl:553
msgid "files"
msgstr "berkas"

#: ../../stattrans.pl:556 ../../stattrans.pl:557 ../../stattrans.pl:558
#: ../../stattrans.pl:559
msgid "bytes"
msgstr "bytes"

#: ../../stattrans.pl:566
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Catatan: daftar halaman diurutkan berdasarkan popularitas. Arahkan kursor ke "
"nama halaman untuk melihat jumlah hit."

#: ../../stattrans.pl:572
msgid "Outdated translations"
msgstr "Terjemahan telah usang"

#: ../../stattrans.pl:574 ../../stattrans.pl:630
msgid "File"
msgstr "Berkas"

#: ../../stattrans.pl:576 ../../stattrans.pl:582
msgid "Diff"
msgstr "Diff"

#: ../../stattrans.pl:578
msgid "Comment"
msgstr "Komentar"

#: ../../stattrans.pl:579
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:584
msgid "Log"
msgstr "Log"

#: ../../stattrans.pl:585
msgid "Translation"
msgstr "Terjemahan"

#: ../../stattrans.pl:586
msgid "Maintainer"
msgstr "Pemelihara"

#: ../../stattrans.pl:588
msgid "Status"
msgstr "Status"

#: ../../stattrans.pl:589
msgid "Translator"
msgstr "Penerjemah"

#: ../../stattrans.pl:590
msgid "Date"
msgstr "Tanggal"

#: ../../stattrans.pl:597
msgid "General pages not translated"
msgstr "Halaman umum tidak diterjemahkan"

#: ../../stattrans.pl:598
msgid "Untranslated general pages"
msgstr "Halaman umum belum diterjemahkan"

#: ../../stattrans.pl:603
msgid "News items not translated"
msgstr "Item berita tidak diterjemahkan"

#: ../../stattrans.pl:604
msgid "Untranslated news items"
msgstr "Item berita belum diterjemahkan"

#: ../../stattrans.pl:609
msgid "Consultant/user pages not translated"
msgstr "Halaman konsultan/pengguna tidak diterjemahkan"

#: ../../stattrans.pl:610
msgid "Untranslated consultant/user pages"
msgstr "Halaman konsultan/pengguna belum diterjemahkan"

#: ../../stattrans.pl:615
msgid "International pages not translated"
msgstr "Halaman internasional tidak diterjemahkan"

#: ../../stattrans.pl:616
msgid "Untranslated international pages"
msgstr "Halaman internasional belum diterjemahkan"

#: ../../stattrans.pl:621
msgid "Translated pages (up-to-date)"
msgstr "Halaman telah diterjemahkan (up-to-date)"

#: ../../stattrans.pl:628 ../../stattrans.pl:778
msgid "Translated templates (PO files)"
msgstr "Tema telah diterjemahkan (berkas PO)"

#: ../../stattrans.pl:629 ../../stattrans.pl:781
msgid "PO Translation Statistics"
msgstr "Statistik Penerjemahan PO"

#: ../../stattrans.pl:632 ../../stattrans.pl:795
msgid "Fuzzy"
msgstr "Ragu-Ragu"

#: ../../stattrans.pl:633
msgid "Untranslated"
msgstr "Belum diterjemahkan"

#: ../../stattrans.pl:634
msgid "Total"
msgstr "Total"

#: ../../stattrans.pl:651
msgid "Total:"
msgstr "Total:"

#: ../../stattrans.pl:685
msgid "Translated web pages"
msgstr "Halaman web telah diterjemahkan"

#: ../../stattrans.pl:688
msgid "Translation Statistics by Page Count"
msgstr "Statistik Terjemahan berdasarkan Jumlah Halaman"

#: ../../stattrans.pl:703 ../../stattrans.pl:749 ../../stattrans.pl:793
msgid "Language"
msgstr "Bahasa"

#: ../../stattrans.pl:704 ../../stattrans.pl:750
msgid "Translations"
msgstr "Terjemahan"

#: ../../stattrans.pl:731
msgid "Translated web pages (by size)"
msgstr "Halaman web telah diterjemahkan (berdasarkan ukuran)"

#: ../../stattrans.pl:734
msgid "Translation Statistics by Page Size"
msgstr "Statistik Terjemahan berdasarkan Ukuran Halaman"

#~ msgid "Created with"
#~ msgstr "Dibuat dengan"
