msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2017-02-16 17:43+0100\n"
"Last-Translator: Andreas Rönnquist <gusnan@openmailbox.org>\n"
"Language-Team: debian-l10n-swedish@lists.debian.org\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Distributör"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Tillåter bidrag"

#: ../../english/CD/vendors/vendors.CD.def:16
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD/BD/USB"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Arkitekturer"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Levererar internationellt"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "Kontakt"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Hemsida för distributör"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "sida"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "e-post"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "inom Europa"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "inom vissa områden"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "källkod"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "och"

#~ msgid "updated monthly"
#~ msgstr "uppdateras månadsvis"

#~ msgid "updated twice weekly"
#~ msgstr "uppdateras två gånger i veckan"

#~ msgid "updated weekly"
#~ msgstr "uppdateras veckovis"

#~ msgid "reseller"
#~ msgstr "återförsäljare"

#~ msgid "reseller of $var"
#~ msgstr "återförsäljare för $var"

#~ msgid "Custom Release"
#~ msgstr "Specialgjord version"

#~ msgid "vendor additions"
#~ msgstr "distributörtillägg"

#~ msgid "contrib included"
#~ msgstr "inkluderar contrib-delen"

#~ msgid "non-free included"
#~ msgstr "inkluderar non-free-delen"

#~ msgid "non-US included"
#~ msgstr "inkluderar non-US-delen"

#~ msgid "Multiple Distribution"
#~ msgstr "Samdistribuerad"

#~ msgid "Vendor Release"
#~ msgstr "Distributörspecifik version"

#~ msgid "Development Snapshot"
#~ msgstr "Utvecklarversion"

#~ msgid "Official DVD"
#~ msgstr "Officiell dvd"

#~ msgid "Official CD"
#~ msgstr "Officiell cd"

#~ msgid "Architectures:"
#~ msgstr "Arkitekturer:"

#~ msgid "DVD Type:"
#~ msgstr "Typ av dvd:"

#~ msgid "CD Type:"
#~ msgstr "Typ av cd:"

#~ msgid "email:"
#~ msgstr "E-post:"

#~ msgid "Ship International:"
#~ msgstr "Levererar internationellt:"

#~ msgid "Country:"
#~ msgstr "Land:"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Tillåter bidrag till Debian:"

#~ msgid "URL for Debian Page:"
#~ msgstr "Webbadress för Debiansida:"

#~ msgid "Vendor:"
#~ msgstr "Distributör:"
