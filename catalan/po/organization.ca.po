# organization webwml Catalan template.
# Copyright (C) 2002, 2003, 2004, 2005 Free Software Foundation, Inc.
# Jordi Mallach <jordi@debian.org>, 2002, 2003.
# Guillem Jover <guillem@debian.org>, 2004-2011, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml\n"
"PO-Revision-Date: 2017-06-19 00:16+0200\n"
"Last-Translator: Guillem Jover <guillem@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "actual"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "membre"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "gestor"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr "Administrador de llançament de la versió estable"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "mag"

#: ../../english/intro/organization.data:39
msgid "chairman"
msgstr "president"

#: ../../english/intro/organization.data:42
msgid "assistant"
msgstr "assistent"

#: ../../english/intro/organization.data:44
msgid "secretary"
msgstr "secretari"

#: ../../english/intro/organization.data:53
#: ../../english/intro/organization.data:63
msgid "Officers"
msgstr "Directors"

#: ../../english/intro/organization.data:54
#: ../../english/intro/organization.data:80
msgid "Distribution"
msgstr "Distribució"

#: ../../english/intro/organization.data:55
#: ../../english/intro/organization.data:224
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:56
#: ../../english/intro/organization.data:227
msgid "Publicity team"
msgstr "Equip de publicitat"

#: ../../english/intro/organization.data:57
#: ../../english/intro/organization.data:295
msgid "Support and Infrastructure"
msgstr "Suport i infraestructura"

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:59
msgid "Debian Pure Blends"
msgstr "Barreges pures de Debian"

#: ../../english/intro/organization.data:66
msgid "Leader"
msgstr "Líder"

#: ../../english/intro/organization.data:68
msgid "Technical Committee"
msgstr "Comitè tècnic"

#: ../../english/intro/organization.data:75
msgid "Secretary"
msgstr "Secretari"

#: ../../english/intro/organization.data:83
msgid "Development Projects"
msgstr "Projectes de desenvolupament"

#: ../../english/intro/organization.data:84
msgid "FTP Archives"
msgstr "Arxius FTP"

#: ../../english/intro/organization.data:86
msgid "FTP Masters"
msgstr "Mestres de FTP"

#: ../../english/intro/organization.data:90
msgid "FTP Assistants"
msgstr "Assistents de FTP"

#: ../../english/intro/organization.data:99
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:103
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:105
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:109
msgid "Individual Packages"
msgstr "Paquets individuals"

#: ../../english/intro/organization.data:110
msgid "Release Management"
msgstr "Gestió de llançaments"

#: ../../english/intro/organization.data:112
msgid "Release Team"
msgstr "Equip de llançament"

#: ../../english/intro/organization.data:125
msgid "Quality Assurance"
msgstr "Control de qualitat"

#: ../../english/intro/organization.data:126
msgid "Installation System Team"
msgstr "Equip del sistema d'instal·lació"

#: ../../english/intro/organization.data:127
msgid "Release Notes"
msgstr "Notes de la versió"

#: ../../english/intro/organization.data:129
msgid "CD Images"
msgstr "Imatges de CD"

#: ../../english/intro/organization.data:131
msgid "Production"
msgstr "Producció"

#: ../../english/intro/organization.data:139
msgid "Testing"
msgstr "Proves"

#: ../../english/intro/organization.data:141
msgid "Autobuilding infrastructure"
msgstr "Infraestructura d'autocompilació"

#: ../../english/intro/organization.data:143
msgid "Wanna-build team"
msgstr "Equip de wanna-build"

#: ../../english/intro/organization.data:151
msgid "Buildd administration"
msgstr "Administració dels dimonis de compilació"

#: ../../english/intro/organization.data:170
msgid "Documentation"
msgstr "Documentació"

#: ../../english/intro/organization.data:175
msgid "Work-Needing and Prospective Packages list"
msgstr "Llista de paquets en perspectiva o en falta de feina"

#: ../../english/intro/organization.data:178
msgid "Live System Team"
msgstr "Equip del sistema autònom"

#: ../../english/intro/organization.data:179
msgid "Ports"
msgstr "Arquitectures"

#: ../../english/intro/organization.data:214
msgid "Special Configurations"
msgstr "Configuracions especials"

#: ../../english/intro/organization.data:217
msgid "Laptops"
msgstr "Portàtils"

#: ../../english/intro/organization.data:218
msgid "Firewalls"
msgstr "Tallafocs"

#: ../../english/intro/organization.data:219
msgid "Embedded systems"
msgstr "Sistemes empotrats"

#: ../../english/intro/organization.data:232
msgid "Press Contact"
msgstr "Contacte de premsa"

#: ../../english/intro/organization.data:234
msgid "Web Pages"
msgstr "Pàgines web"

#: ../../english/intro/organization.data:244
msgid "Planet Debian"
msgstr "Planeta Debian"

#: ../../english/intro/organization.data:249
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:253
msgid "Debian Women Project"
msgstr "Projecte Debian dona"

#: ../../english/intro/organization.data:261
msgid "Anti-harassment"
msgstr ""

#: ../../english/intro/organization.data:267
msgid "Events"
msgstr "Esdeveniments"

#: ../../english/intro/organization.data:273
msgid "DebConf Committee"
msgstr "Comitè de DebConf"

#: ../../english/intro/organization.data:280
msgid "Partner Program"
msgstr "Programa de socis"

#: ../../english/intro/organization.data:285
msgid "Hardware Donations Coordination"
msgstr "Coordinació de donacions de maquinari"

#: ../../english/intro/organization.data:298
msgid "User support"
msgstr "Suport d'usuari"

#: ../../english/intro/organization.data:365
msgid "Bug Tracking System"
msgstr "Sistema de Seguiment d'Errors"

#: ../../english/intro/organization.data:370
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Administració i arxius de les llistes de correu"

#: ../../english/intro/organization.data:378
#, fuzzy
#| msgid "New Maintainers Front Desk"
msgid "New Members Front Desk"
msgstr "Recepció de nous mantenidors"

#: ../../english/intro/organization.data:384
msgid "Debian Account Managers"
msgstr "Administradors dels comptes de Debian"

#: ../../english/intro/organization.data:389
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:390
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Mantenidors de l'anell de claus (PGP i GPG)"

#: ../../english/intro/organization.data:393
msgid "Security Team"
msgstr "Equip de seguretat"

#: ../../english/intro/organization.data:406
msgid "Consultants Page"
msgstr "Pàgina de consultors"

#: ../../english/intro/organization.data:411
msgid "CD Vendors Page"
msgstr "Pàgina de proveïdors de CD"

#: ../../english/intro/organization.data:414
msgid "Policy"
msgstr "Política"

#: ../../english/intro/organization.data:419
msgid "System Administration"
msgstr "Administració del sistema"

#: ../../english/intro/organization.data:420
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Aquesta és l'adreça a utilitzar quan es troben problemes en una màquina de "
"Debian, incloent problemes amb contrasenyes o si necessiteu la instal·lació "
"d'un paquet."

#: ../../english/intro/organization.data:430
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Si teniu problemes de hardware en màquines de Debian, si us plau mireu la "
"pàgina de <a href=\"https://db.debian.org/machines.cgi\">Màquines de Debian</"
"a>, hauria de contenir informació de l'administrador de cada màquina."

#: ../../english/intro/organization.data:431
msgid "LDAP Developer Directory Administrator"
msgstr "Administrador del directori LDAP de desenvolupadors"

#: ../../english/intro/organization.data:432
msgid "Mirrors"
msgstr "Rèpliques"

#: ../../english/intro/organization.data:437
msgid "DNS Maintainer"
msgstr "Mantenidor del DNS"

#: ../../english/intro/organization.data:438
msgid "Package Tracking System"
msgstr "Sistema de seguiment de paquets"

#: ../../english/intro/organization.data:440
msgid "Auditor"
msgstr "Auditor"

#: ../../english/intro/organization.data:446
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:449
msgid "Alioth administrators"
msgstr "Administració d'Alioth"

#: ../../english/intro/organization.data:462
msgid "Debian for children from 1 to 99"
msgstr "Debian per a nens de 1 a 99"

#: ../../english/intro/organization.data:465
msgid "Debian for medical practice and research"
msgstr "Debian per a la pràctica i recerca medica"

#: ../../english/intro/organization.data:468
msgid "Debian for education"
msgstr "Debian per a l'educació"

#: ../../english/intro/organization.data:473
msgid "Debian in legal offices"
msgstr "Debian a les oficines de dret"

#: ../../english/intro/organization.data:477
msgid "Debian for people with disabilities"
msgstr "Debian per a gent amb discapacitats"

#: ../../english/intro/organization.data:481
#, fuzzy
#| msgid "Debian for medical practice and research"
msgid "Debian for science and related research"
msgstr "Debian per a la pràctica i recerca medica"

#~ msgid "Publicity"
#~ msgstr "Publicitat"

#, fuzzy
#~| msgid "Debian Maintainer Keyring Team"
#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Equip de l'anell de claus dels mantenidors de Debian"

#~ msgid "Volatile Team"
#~ msgstr "Equip de llançament da la versió volàtil"

#~ msgid "Vendors"
#~ msgstr "Venedors"

#~ msgid "APT Team"
#~ msgstr "Equip APT"

#~ msgid "Release Assistants"
#~ msgstr "Assistents de llançament"

#~ msgid "Release Assistants for ``stable''"
#~ msgstr "Assistents de llançaments per a «stable»"

#~ msgid "Release Wizard"
#~ msgstr "Mag de llançaments"

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "Distribució multimedia de Debian"

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Aquest encara no és un projecte intern oficial de Debian però ha anunciat "
#~ "la intenció de ser-ho."

#~ msgid "Mailing List Archives"
#~ msgstr "Arxius de les llistes de correu"

#~ msgid "Installation System for ``stable''"
#~ msgstr "Sistema d'instal·lació per a «stable»"

#~ msgid "Debian GNU/Linux for Enterprise Computing"
#~ msgstr "Debian GNU/Linux per a la computació d'empreses"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian per organitzacions sense anim de lucre"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "El sistema operatiu universal com el seu escriptori"

#~ msgid "Accountant"
#~ msgstr "Contable"

#~ msgid "Key Signing Coordination"
#~ msgstr "Coordinació de signatura de claus"

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Els noms individuals dels administradors dels dimonis de compilació es "
#~ "poden trobar a <a href=\"http://www.buildd.net\">http://www.buildd.net</"
#~ "a>. Seleccioneu una arquitecture i una distribució per veure els dimonis "
#~ "de compilació disponibles i els seus administradors."

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Els administradors responsables dels dimonis de compilació per a una "
#~ "arquitectura particular, es poden contactar a <genericemail arch@buildd."
#~ "debian.org>, per exemple <genericemail i386@buildd.debian.org>."

#~ msgid "Marketing Team"
#~ msgstr "Equip de màrqueting"

#~ msgid "Handhelds"
#~ msgstr "Ordinadors de ma"

#~ msgid "Release Team for ``stable''"
#~ msgstr "Equip de llançaments per a «stable»"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Distribució de Debian adaptada"

#~ msgid "Security Audit Project"
#~ msgstr "Projecte d'auditoria de seguretat"

#~ msgid "Testing Security Team"
#~ msgstr "Equip de seguretat de Proves"
