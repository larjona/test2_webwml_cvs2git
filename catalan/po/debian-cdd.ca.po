# debian-cdd webwml Catalan template.
# Copyright (C) 2004 Free Software Foundation, Inc.
# Guillem Jover <guillem@debian.org>, 2004, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml\n"
"PO-Revision-Date: 2017-06-19 00:21+0200\n"
"Last-Translator: Guillem Jover <guillem@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/debian-cdd.wml:28
#: ../../english/template/debian/debian-cdd.wml:68
msgid "Debian-Med"
msgstr "Debian-Med"

#: ../../english/template/debian/debian-cdd.wml:31
#: ../../english/template/debian/debian-cdd.wml:72
msgid "Debian-Jr"
msgstr "Debian-Jr"

#: ../../english/template/debian/debian-cdd.wml:34
#: ../../english/template/debian/debian-cdd.wml:76
msgid "Debian-Lex"
msgstr "Debian-Lex"

#: ../../english/template/debian/debian-cdd.wml:36
msgid ""
"The list below includes various software projects which are of some interest "
"to the <get-var cdd-name /> Project. Currently, only a few of them are "
"available as Debian packages. It is our goal, however, to include all "
"software in <get-var cdd-name /> which can sensibly add to a high quality "
"Custom Debian Distribution."
msgstr ""
"La llista següent inclou diversos projectes de programari que són d'algun "
"interès per al projecte <get-var cdd-name />. Actualment només alguns estan "
"disponibles com a paquets de Debian. No obstant, el nostre objectiu és "
"incloure tot el programari a <get-var cdd-name /> que es pugui afegir de "
"forma raonable a una distribució de Debian adaptada de qualitat."

#: ../../english/template/debian/debian-cdd.wml:41
msgid ""
"For a better overview of the project's availability as a Debian package, "
"each head row has a color code according to this scheme:"
msgstr ""
"Per a una millor inspecció rapida de la disponibilitat en forma de paquet de "
"Debian del projecte, cada fila té un codi de color seguint aquest esquema:"

#: ../../english/template/debian/debian-cdd.wml:47
msgid ""
"Green: The project is <a href=\"<get-var url />\">available as an official "
"Debian package</a>"
msgstr ""
"Verd: El projecte està <a href=\"<get-var url />\">disponible com a paquet "
"oficial de Debian</a>"

#: ../../english/template/debian/debian-cdd.wml:54
msgid ""
"Yellow: The project is <a href=\"<get-var url />\">available as an "
"unofficial Debian package</a>"
msgstr ""
"Groc: El projecte està <a href=\"<get-var url />\">disponible com a paquet "
"de Debian no oficial</a>"

#: ../../english/template/debian/debian-cdd.wml:61
msgid ""
"Red: The project is <a href=\"<get-var url />\">not (yet) available as a "
"Debian package</a>"
msgstr ""
"Vermell: El projecte està <a href=\"<get-var url />\">encara no disponible "
"com a paquet de Debian</a>"

#: ../../english/template/debian/debian-cdd.wml:79
msgid ""
"If you discover a project which looks like a good candidate for <get-var cdd-"
"name /> to you, or if you have prepared an unofficial Debian package, please "
"do not hesitate to send a description of that project to the <a href=\"<get-"
"var cdd-list-url />\"><get-var cdd-name /> mailing list</a>."
msgstr ""
"Si descobriu un projecte que us sembli un bon candidat per a <get-var cdd-"
"name /> o si heu preparat un paquet de Debian no oficial, si us plau no "
"dubteu a enviar una descripció d'aquest projecte a la <a href=\"<get-var cdd-"
"list-url />\"><get-var cdd-name /> llista de correu</a>."

#: ../../english/template/debian/debian-cdd.wml:84
msgid "Official Debian packages"
msgstr "Paquets oficials de Debian"

#: ../../english/template/debian/debian-cdd.wml:88
msgid "Unofficial Debian packages"
msgstr "Paquets no oficials de Debian"

#: ../../english/template/debian/debian-cdd.wml:92
msgid "Debian packages not available"
msgstr "Paquets de Debian no disponibles"

#: ../../english/template/debian/debian-cdd.wml:96
msgid "There are no projects which fall into this category."
msgstr "No hi ha cap projecte dins d'aquesta categoria."

#: ../../english/template/debian/debian-cdd.wml:100
msgid "No homepage known"
msgstr "Sense pàgina web coneguda"

#: ../../english/template/debian/debian-cdd.wml:104
msgid "License:"
msgstr "Llicencia:"

#: ../../english/template/debian/debian-cdd.wml:108
msgid "Free"
msgstr "Lliure"

#: ../../english/template/debian/debian-cdd.wml:112
msgid "Non-Free"
msgstr "No-lliure"

#: ../../english/template/debian/debian-cdd.wml:116
msgid "Unknown"
msgstr "Desconegut"

#: ../../english/template/debian/debian-cdd.wml:120
msgid "Official Debian package"
msgstr "Paquet oficial de Debian"

#: ../../english/template/debian/debian-cdd.wml:124
msgid "Unofficial Debian package"
msgstr "Paquet no oficial de Debian"

#: ../../english/template/debian/debian-cdd.wml:128
msgid "Debian package not available"
msgstr "Paquet de Debian no disponible"
