#use wml::debian::translation-check translation="97f824ca074e7cbb5879f7ba810e82d551fb87d6"

<hrline/>
<address>
Администратори на системата за следене на грешките на Debian &lt;<email "owner@bugs.debian.org">&gt;

<p>Система за следене на грешките в Debian<br/>
Copyright &copy; 1999 Darren O. Benham, 1997, 2003 nCipher Corporation Ltd,
1994-1997 Ian Jackson.</p>
</address>
